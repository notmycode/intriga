<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gift_model extends MY_Model
{
    public $protected_attributes = array('password_confirm');

    public function user($user_id)
    {

        $this->db->select('
            user.id as user_id, 
            user.first_name as user_last_name, 
            user.last_name as user_first_name, 
            user.image as user_image, 
            gift.image as gift_image,
            gifts_relation.text as gift_text
        ');

        $this->db->where(array(
            'gifts_relation.user_id' => $user_id,
            'gifts_relation.status' => 1
        ));

        $this->db->join('users user', 'user.id = gifts_relation.sender_id');
        $this->db->join('gifts gift', 'gift.id = gifts_relation.gift_id');

        $query = $this->db->get('gifts_relation');

        return $query;
    }

    public function send($reciver, $sender, $gift_id, $text = NULL)
    {
        $gift = $this->get($gift_id);
        
        $this->load->model('User_model', 'user');
        $this->load->model('Finance_model', 'finance');
        
        $user = $this->user->get($sender);
       
        
        if( $user->balance >= $gift->price && $gift->status == 1){
            $this->user->update($sender, array('balance' => $user->balance - $gift->price));
            
            //Add spendings
            if(is_numeric($this->finance->add_spending($sender, 'gift', $gift->price, $gift->id))){
                
                $this->db->insert('gifts_relation', 
                array(
                    'gift_id' => (integer)$gift->id,
                    'sender_id' => (integer)$sender,
                    'user_id' => (integer)$reciver,
                    'status' => (integer)1,
                    'text' => (string)$text
                ));
                
                return response_data(['Gift sended'], 200);
            }else{
                return response_error(array('Error happen'), 401);
            }
            
            
           
        }else{
             return response_error(array('You don\'t have enought money'), 401);
        }
    }

}