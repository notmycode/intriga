<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_model extends MY_Model
{
    public $before_create = array( 'created_at', 'updated_at' );
    public $before_update = array( 'updated_at' );
    protected $soft_delete = FALSE;

    public function wall($userID)
    {
        $this->db->select('users.id, users.first_name, users.last_name, users.image, posts.text, posts.attachments');
        $this->db->from('posts');
        $this->db->where('user_id', $userID);
        $this->db->where('status', 1);
        $this->db->join('users', 'users.id = posts.user_id');
        $query = $this->db->get();
        return $query->num_rows() > 0 ?  $query : array();
    }

}