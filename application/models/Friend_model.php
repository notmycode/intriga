<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Friend_model extends MY_Model
{
    public $before_create = array( 'created_at', 'updated_at' );
    public $before_update = array( 'updated_at' );

    public $belongs_to = array( 'user' => array( 'primary_key' => 'user_init' ), 'user' => array('primary_key' => 'user_reciver') );

    /*
     * Is frineds request exists already
     * */
    public function request_exists($users = array())
    {

        $where_au = "(user_init = '".$users['user']."' AND user_reciver = '".$users['user_id']."' )
        OR (user_init = '".$users['user_id']."' AND user_reciver = '".$users['user']."')";


        $this->db->where($where_au);

        $query = $this->db->get('friends');

        return $query->num_rows() > 0 ? TRUE : FALSE;
    }

    /*
     * Sending request
     * */
    public function send_request($users = array())
    {
        if($this->request_exists($users) == FALSE){
            $data = array(
                'user_init' => $users['user'],
                'user_reciver' => $users['user_id'],
                'status' => 0
            );
            return $this->insert($data);
        }else{
            return FALSE;
        }
    }

    public function approve_request($id, $user)
    {
        $data = array(
            'status' => 1
        );

        $this->db->where('user_reciver', $user);
        $this->db->where('status', 0);
        $this->db->where('id', $id);

        $this->db->update('friends', $data);

        return $this->db->affected_rows() > 0 ? TRUE : FALSE;
    }

    public function input_requests($userID)
    {
        $this->db->select('user.id, user.first_name, user.last_name, user.image, friends.id as request_id');
        $this->db->where('friends.user_reciver', $userID);
        $this->db->where('friends.status', 0);
        $this->db->join('users user', 'user.id = friends.user_init');
        $query = $this->db->get('friends');

        return $query->num_rows() > 0 ? $query : FALSE;
    }

    public function decline($requestID, $userID)
    {
        $this->db->where('id', $requestID);
        $this->db->where('user_reciver', $userID);
        $this->db->where('status', 0);

        $data = array(
            'status' => 2
        );

        $this->db->update('friends', $data);

        return $this->db->affected_rows() > 0 ? TRUE : FALSE;
    }

    public function remove($userID, $user)
    {
        $where_au = "(user_init = '".$userID."' AND user_reciver = '".$user."' )
        OR (user_init = '".$user."' AND user_reciver = '".$userID."') AND (status = 1)";

        $this->db->where($where_au);

        $this->db->delete('friends');

        return $this->db->affected_rows() > 0 ? TRUE : FALSE;

    }

    public function all($userID)
    {
        $this->db->select('user.id, user.first_name, user.last_name, user.image, friends.id as request_id');
        $this->db->where('friends.status', 1);
        $this->db->where('user_reciver', $userID);
        $this->db->or_where('user_init', $userID);

        $this->db->join('users user', 'user.id = friends.user_init OR user.id = friends.user_reciver AND
         friends.user_init !='.$userID.' AND friends.user_reciver !='.$userID);


        return $this->db->get('friends');

    }
}