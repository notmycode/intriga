<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blacklist_model extends MY_Model
{
    public function add($user_id, $blocked_id)
    {
        $data = array(
            'user_id' => $user_id,
            'blocked_id' => $blocked_id
        );
        
        if(is_integer($this->insert($data)))
            return response_success (['User added to your blacklist'], 201);
        
        return response_error(['Server Error'], 500);
    }
    
    public function remove($user_id, $blocked_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('blocked_id', $blocked_id);
        
        if($this->db->delete('blacklists') == 1){
            return response_success(['User remove from blacklist'], 200);
        }else{
            return response_error(['Server Error'], 500);
        }
    }
    
    public function get($user)
    {
        $this->db->select(
                  'user.id,'
                . 'user.first_name,'
                . 'user.last_name,'
                . 'user.image'
                );
        
        $this->db->where('blacklists.user_id', $user);
        $this->db->join('users user', 'user.id=blacklists.blocked_id');
        $this->db->order_by('user.id','asc');  
        $rows = $this->db->get('blacklists');
        
        if($rows){
            return response_data($rows->result_array(), 200);
        }else{
            return response_error(['Server Error'], 500);
        }
        
    }
}