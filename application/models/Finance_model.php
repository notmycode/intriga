<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Finance_model extends MY_Model
{
    public function add_spending($user_id, $product, $price, $product_id)
    {
        return $this->insert(array(
            'user_id' => $user_id,
            'product' => $product,
            'sum' => $price,
            'type' => 'spending',
            'additional' => json_encode(array(
                'gift_id' => $product_id
            ))
        ));
    }
}