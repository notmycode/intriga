<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image_model extends MY_Model
{
    public function albums($album_id)
    {
        $this->db->where('album_id', $album_id);
        $result = $this->db->get('images');
        return $result->num_rows() > 0 ? $result : FALSE;
    }
}