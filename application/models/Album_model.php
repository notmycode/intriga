<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Album_model extends MY_Model
{
    public $before_create = array('created_at', 'updated_at');
    public $before_update = array('updated_at');

    public $belongs_to = array('user' => array('primary_key' => 'user_id'));

    public function add($data, $user)
    {
        $album = array(
            'name' => $data['name'],
            'user_id' => $user,
            'visibility' => $data['visibility'],
            'description' => $data['description']
        );

        return $this->insert($album);
    }

    public function update_album($data, $album_id, $user)
    {

        $where = "( user_id = '".$user."' ) AND ( id = '".$album_id."' )";

        $this->db->where($where);

        $this->db->update('albums', $data);

        return $this->db->affected_rows() > 0 ? TRUE : FALSE;
    }

    public function delete_album($album_id, $user)
    {
        $where = "( user_id = '".$user."' ) AND ( id = '".$album_id."' ) AND ( visibility  != '4' )";

        $this->db->where($where);

        $data = array(
            'visibility' => 4
        );

        $this->db->update('albums', $data);

        return $this->db->affected_rows() > 0 ? TRUE : FALSE;
    }

    public function get_album($album_id)
    {
        $album = $this->get($album_id);
        if($album && $album->visibility != 4){
            $this->load->model('Image_model', 'images');
            $photos = $this->images->albums($album_id);
            if($photos){
                $album->photos = array(
                    'total' => $photos->num_rows(),
                    'records' => $photos->result_array()
                );
            }else{
                $album->photos = array();
            }

            return $album;

        }else{
            return FALSE;
        }
    }

    public function all($user_id)
    {
        $this->db->select('
            albums.name,
            albums.user_id,
            albums.id,
            albums.description,
            albums.visibility,
            COUNT(images.id) as images_total
        ');


        $this->db->where('user_id', $user_id);
        $this->db->where('visibility !=', 4);

        $this->db->join('images', 'images.album_id = albums.id', 'left');

        $this->db->order_by('albums.id','DESC');

        $this->db->group_by('albums.id');

        $query = $this->db->get('albums');

        return $query->result_array();
    }

    public function photos($user_id)
    {

        $albums_ids = $this->db->where('user_id', $user_id)->where('visibility !=', 4)->select('id')->get('albums');
        $ids = array();
        if($albums_ids->num_rows() > 0){
            foreach($albums_ids->result_array() as $album){
                array_push($ids, $album['id']);
            }

            $query = $this->db->where_in($ids)->select('src, album_id')->get('images');

            if($query->num_rows() > 0){
                return $query->result_array();
            }else{
                return array();
            }

        }else{
            return array();
        }

    }


}
