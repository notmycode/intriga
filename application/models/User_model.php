<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends MY_Model
{
    public $protected_attributes = array('password_confirm');
    
    public function top()
    {
        $this->db->where('status', 1);
        $this->db->order_by('rating', 'ASC');
        return $this->db->get('users');
    }
}