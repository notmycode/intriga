<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation
{

    private $_standard_date_format = 'Y-m-d';


    function valid_date($str, $format = NULL)
    {
        if($format == NULL)
        {
            $format = $this->_standard_date_format;
        }

        $d = DateTime::createFromFormat($format, $str);

        return $d && $d->format($format) === $str;

    }

    public function is_exists_db($str, $field)
    {
        $this->CI =& get_instance();

        sscanf($field, '%[^.].%[^.]', $table, $field);

        return isset($this->CI->db)
            ? ($this->CI->db->limit(1)->get_where($table, array($field => $str))->num_rows() === 1)
            : FALSE;
    }

    public function must_not_be_like($str, $field)
    {
        return ($str != $this->_field_data[$field]['postdata'] ? TRUE : FALSE);
    }



    public function in_blacklist($str, $field)
    {

        if(isset($this->_field_data[$field]))
        {
            $this->CI =& get_instance();
            $query = $this->CI->db->limit(1)->get_where('blacklists', array('user_id' => $this->_field_data[$field]['postdata'], 'blocked_id' => $str))->num_rows() > 0;

            return ! $query;
        }
    }

    public function test($str){
        return $str == 'test';
    }

    public function if_empty_become_required($str, $field)
    {
        if(isset($this->_field_data[$field]) == TRUE && mb_strlen($this->_field_data[$field]['postdata']) > 0 ){
            return TRUE;
        }else{
            if( mb_strlen($str) > 0 ){
                return TRUE;
            }else{
                return FALSE;
            }
        }
    }
}