<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Api extends REST_Controller
{
    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->checkForToken();
    }

    private $userID = FALSE;

    function registration_post()
    {
        $data = [
            'tel' => $this->post('tel'),
            'bday' =>  $this->post('bday'),
            'first_name' => $this->post('first_name'),
            'last_name' => $this->post('last_name'),
            'password' => $this->post('password'),
            'status' => 1,
            'role' => 1,
            'password_confirm' => $this->post('password_confirm')
        ];

        $this->form_validation->set_data($data);

        if( $this->form_validation->run('registration') )
        {
            $this->load->library('encryption');
            $data['password'] = $this->encryption->encrypt($data['password']);

            $this->load->model('User_model', 'user');

            if( $this->user->insert($data) )
            {
                $this->response(array('success' => 'You have sign up.'), 201);
            }else{
                $this->response(null, 422);
            }
        }else{
            $this->response(array('errors' => $this->form_validation->error_array() ), REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    function login_post()
    {
        $data = [
            'tel' => $this->post('tel'),
            'password' => $this->post('password')
        ];

        $this->form_validation->set_data($data);

        if($this->form_validation->run('auth'))
        {
            $this->load->model('User_model', 'user');

            $user = $this->user->get_by('tel', $data['tel']);

            if($user){
                $this->load->library('encryption');
                $password = $this->encryption->decrypt($user->password);
                if($password == $data['password']){
                    $this->response(array('success' => 'You have sign in.', 'token' => $this->encryption->encrypt($user->id)), 200);
                }else{
                    $this->response(array('errors' => array('password' => 'Password is incorrect')), REST_Controller::HTTP_BAD_REQUEST);
                }
            }else{
                $this->response(array('errors' => ['User not found']), REST_Controller::HTTP_BAD_REQUEST);
            }
            $this->response(array('success' => 'You have sign in.', 'data' => $user), 200);
        }else{
            $this->response(array('errors' => $this->form_validation->error_array()), REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function create_post_wall_post()
    {
        if(!empty($this->userID)){
            $this->response($this->userID, 200);
        }else{
            $this->response(array('errors' => ['asd'] ), REST_Controller::HTTP_BAD_REQUEST);
        }
    }


    /*
     * Checking if current request has token in header
     *
     * */

    private function checkForToken()
    {
        $token = $this->input->get_request_header('X-API-KEY', TRUE);
        if($token){
            if(stripos($token, 'Bearer') !== FALSE){
                $token = str_replace('Bearer', '', $token);
                $this->load->library('encryption');
                $id = $this->encryption->decrypt(trim($token));
                if(!$id){
                    $this->response(array('errors' => ['Token is invalid!']), REST_Controller::HTTP_UNAUTHORIZED);
                }else{
                    $this->userID = $id;
                }
            }else{
                $this->response(array('errors' => ['Token is invalid!']), REST_Controller::HTTP_UNAUTHORIZED);
            }
        }
    }


}