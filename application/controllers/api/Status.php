<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Status extends REST_Controller
{
    function __construct($config = 'rest')
    {
        parent::__construct($config);
    }

    public function create_post()
    {
        $data = array(
            'name' => $this->post('name'),
            'price' =>  empty($this->post('price')) == TRUE ? 0 : $this->post('price')
        );

        $this->form_validation->set_data($data);

        if($this->form_validation->run('create_status')){
            $this->load->model('status_model', 'status');
            $this->status->insert($data);
            $this->response(null, 201);
        }else{
            $this->response($this->form_validation->error_array(), 422);
        }

    }

    public function single_get()
    {}


}