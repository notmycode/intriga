<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class ApiAuth extends REST_Controller
{
    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->checkForToken();
    }

    private $userID = FALSE;


    public function create_post_wall_post($wallID)
    {
        $data = array(
            'author_id'   => $this->userID,
            'user_id'     => $wallID,
            'status'      => 1,
            'text'        => $this->post('text'),
            'attachments' => $this->post('attachments')
        );

        //checking if one of two fields is filled
        mb_strlen($data['text']) === 0 && mb_strlen($data['attachments']) == 0 ?
            $this->response(array( 'errors' => 'Field required' ), REST_Controller::HTTP_BAD_REQUEST) : '';

        $this->form_validation->set_data($data);

        if($this->form_validation->run('create_wall_post')){
            $this->load->model('Post_model', 'post');
            $result = $this->post->insert($data);
            if($result){
                $this->response(array('message' => 'Post Created!'), REST_Controller::HTTP_CREATED);
            }else{
                $this->response(array('errors' => $this->form_validation->error_array()), REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }
        }else{
            $this->response(array('errors' => $this->form_validation->error_array()), REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    /*
     *  Getting wall posts
     * */
    public function wall_posts_get($wallID)
    {
        $data = array(
            'user_id' => $wallID,
            'user' => $this->userID
        );

        $this->form_validation->set_data($data);

        if($this->form_validation->run('get_wall_posts'))
        {
            $this->load->model('Post_model', 'post');
            $result = $this->post->wall($data['user_id']);
            $this->response(array('total' =>  $result->num_rows, 'records' => $result->result()), 200);
        }else{
            $this->response(array('errors' => $this->form_validation->error_array()), REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function wall_post_delete($postID)
    {
        $data = array(
            'post_id' => $postID,
            'user_id' => $this->userID
        );

        $this->form_validation->set_data($data);

        if($this->form_validation->run('delete_wall_post'))
        {
            $this->load->model('Post_model', 'post');

            $post = $this->post->get($postID);

            //checking if user have premission to delete this post
            empty($post) == FALSE && $post->author_id != $data['user_id'] && $post->user_id != $data['user_id'] ?
                $this->response(array('errors' => ['You do not have premissions']), REST_Controller::HTTP_BAD_REQUEST)
            : NULL;

            if( $this->post->delete($data['post_id']) ){
                $this->response(array('message' => 'Post deleted'), 200);
            }else{
                $this->response(array('errors' => ['You do not have premissions']), REST_Controller::HTTP_BAD_REQUEST);
            }
        }else{
            $this->response(array('errors' => $this->form_validation->error_array()), REST_Controller::HTTP_BAD_REQUEST);
        }


    }



    /*
     * Send friends request
     * */
    public function add_friend_post()
    {
        $data = array(
            'user_id' => $this->post('user_id'),
            'user' => $this->userID
        );

        $this->form_validation->set_data($data);

        if($this->form_validation->run('add_friend')){
            $this->load->model('Friend_model', 'friend');
            if($this->friend->send_request($data))
            {
                $this->response(array('message' => 'Friends request sended'), 201);
            }else{
                $this->response(array('errors' => ['Request sended already']), REST_Controller::HTTP_BAD_REQUEST);
            }
        }else{
            $this->response(array('errors' => $this->form_validation->error_array()), REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    /*
     * Approve friend request
     * */
    public function approve_friend_put()
    {
        $data = array(
            'request_id' => $this->put('request_id'),
            'user_id' => $this->userID
        );

        $this->form_validation->set_data($data);

        if($this->form_validation->run('approve_friend')){
            $this->load->model('Friend_model', 'friend');
            $approve = $this->friend->approve_request($data['request_id'], $data['user_id']);

            if($approve){
                $this->response(
                    array(
                        'message' => 'Request approved'
                    ),
                    200
                );
            }else{
                $this->response(array('errors' => ['Server error happen']), REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }
        }else{
            $this->response(array('errors' => $this->form_validation->error_array()), REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    /*
     * Getting users friends requests
     * */
    public function friends_requests_get()
    {
        $data = array(
            'user_id' => $this->userID,
        );

        $this->load->model('Friend_model', 'friend');

        $requests = $this->friend->input_requests($data['user_id']);

        $this->response(
            array(
                'total' => $requests->num_rows(),
                'records' => $requests->result_array()
            ),
            200
        );

    }

    public function friends_get($userID = NULL)
    {
        $data = array(
            'user' => $userID == NULL ? $this->userID : $userID,
            'user_id' => $this->userID
        );

        $this->form_validation->set_data($data);

        if($this->form_validation->run('get_friends'))
        {
            $this->load->model('Friend_model', 'friend');
            $result = $this->friend->all($data['user']);

            if($result){
                $this->response(
                    array(
                        'total' => $result->num_rows(),
                        'records' => $result->result_array()
                    ),
                    200
                );
            }
        }else{
            $this->response(
                array(
                    'errors' => $this->form_validation->error_array()
                ),
                422
            );
        }
    }

    /*
     * Declime friends request
     * */
    public function decline_friend_post()
    {
        $data = array(
            'request_id' => $this->post('request_id'),
            'user_id' => $this->userID
        );

        $this->form_validation->set_data($data);

        if(!$this->form_validation->run('declime_friend')){
            $this->response(
                array(
                    'errors' => $this->form_validation->error_array()
                ),
                422
            );
        }

        $this->load->model('Friend_model', 'friend');
        $result = $this->friend->decline($data['request_id'], $data['user_id']);

        if($result){
            $this->response(
                array(
                    'message' => ['Request declimed']
                ),
                200
            );
        }else{
            $this->response(
                array(
                    'errors' => ['Server error happen']
                ),
                500
            );
        }

    }

    /*
     * Remove from friends
     * */

    public function remove_friend_delete($user_id)
    {
        $data = array(
            'user_id' => $user_id,
            'user' => $this->userID
        );

        $this->form_validation->set_data($data);

        if(!$this->form_validation->run('remove_friend'))
        {
            $this->response(array(
                'errors' => $this->form_validation->error_array()
            ), 422);
        }

        $this->load->model('Friend_model', 'friend');
        $result = $this->friend->remove($data['user_id'], $data['user']);
        if($result){
            $this->response(array(
                'message' => ['User deleted from friends']
            ), 200);
        }else{
            $this->response(array(
                'errors' => ['Can not delete user from friends']
            ), 422);
        }
    }

    /*
     * Albums create
     * */
    public function album_create_post()
    {
        $data = array(
            'name' => $this->post('name'),
            'description' => $this->post('description'),
            'visibility' => $this->post('visibility')
        );

        $this->form_validation->set_data($data);

        if(!$this->form_validation->run('create_album'))
        {
            $this->response(array(
                'errors' => $this->form_validation->error_array()
            ), 422);
        }

        $this->load->model('Album_model', 'album');

        if($this->album->add($data, $this->userID))
        {
            $this->response(array(
                'message' => 'album created'
            ), 201);
        }else{
            $this->response(array(
                'errors' => ['Server error']
            ), 422);
        }
    }

    public function album_update_put($album_id)
    {
        $data = array(
            'name' => $this->put('name'),
            'description' => $this->put('description'),
            'visibility' => $this->put('visibility')
        );

        $this->form_validation->set_data($data);

        if(!$this->form_validation->run('create_album'))
        {
            $this->response(array(
                'errors' => $this->form_validation->error_array()
            ), 422);
        }

        $this->load->model('Album_model', 'album');
        $result =  $this->album->update_album($data, $album_id, $this->userID);

        if($result){
            $this->response(array(
                'message' => ['Album data was update']
            ), 200);
        }else{
            $this->response(array(
                'errors' => ['Server error']
            ), 422);
        }
    }

    public function album_remove_delete($album_id)
    {
        $data = array(
            'album_id' => $album_id
        );

        $this->form_validation->set_data($data);

        if(!$this->form_validation->run('remove_album'))
        {
            $this->response(array(
                'errors' => $this->form_validation->error_array()
            ), 422);
        }

        $this->load->model('Album_model', 'album');

        if($this->album->delete_album($data['album_id'], $this->userID)){
            $this->response(array(
                'message' => ['Album removed']
            ), 200);
        }else{
            $this->response(array(
                'errors' => ['Server error']
            ), 422);
        }
    }

    public function album_get_get($album_id)
    {
        $data = array(
            'album_id' => $album_id
        );

        $this->form_validation->set_data($data);

        if(!$this->form_validation->run('get_album'))
        {
            $this->response(array(
                'errors' => $this->form_validation->error_array()
            ), 422);
        }

        $this->load->model('Album_model', 'album');

        $album = $this->album->get_album($album_id);

        if($album){
            print_r($album);
        }else{
            $this->response(array(
                'errors' => ['Server Error']
            ), 422);
        }
    }

    public function get_albums_get($user_id)
    {
        $data = array(
            'user_id' => $user_id,
            'user' => $this->userID
        );

        $this->form_validation->set_data($data);

        if(!$this->form_validation->run('get_albums'))
        {
            $this->response(array(
                'errors' => $this->form_validation->error_array()
            ), 422);
        }

        $this->load->model('Album_model', 'album');
        $albums = $this->album->all($data['user_id']);

        if(is_array($albums)){
            $this->response(array(
                'records' => $albums
            ), 200);
        }else{
            $this->response(array(
                'errors' => ['Server Error']
            ), 422);
        }

    }

    public function get_photos_get($user_id)
    {
        $data = array(
            'user_id' => $user_id,
            'user' => $this->userID
        );

        $this->form_validation->set_data($data);

        if(!$this->form_validation->run('get_albums'))
        {
            $this->response(array(
                'errors' => $this->form_validation->error_array()
            ), 422);
        }

        $this->load->model('Album_model', 'album');
        print_r($this->album->photos($user_id));
    }


    /*
     * Get gifts list
     * */
    public function gifts_get()
    {
        $this->load->model('Gift_model', 'gifts');
        $gifts = $this->gifts->get_many_by(array(
            'status =' => '1'
        ));

        if(is_array($gifts)){
            $this->response(array(
                'records' => $gifts
            ), 200);
        }else{
            $this->response(array(
                'message' => ['Server Error']
            ), 422);
        }

    }

    /*
     * Get user gifts
     * */
    public function user_gifts_get($user_id)
    {
        $data = array(
            'user_id' => $user_id,
            'user' => $this->userID
        );

        $this->form_validation->set_data($data);

        if(!$this->form_validation->run('get_user_gifts'))
        {
            $this->response(array(
                'errors' => $this->form_validation->error_array()
            ), 422);
        }

        $this->load->model('Gift_model', 'gifts');

        $gifts = $this->gifts->user($data['user_id'])->result();

        if(is_array($gifts)){
            $this->response(array(
                'records' => $gifts,
            ), 200);
        }else{
            $this->response(array(
                'message' => ['Server Error']
            ), 422);
        }
    }

    /*
     * Send gift to user
     * */
    public function send_gift_post()
    {
        $data = array(
            'sender_id' => $this->userID,
            'user_id' => $this->post('user_id'),
            'gift_id' => $this->post('gift_id'),
            'text' => $this->post('text')
        );
        
        $this->form_validation->set_data($data);
        
        if(!$this->form_validation->run('send_gift'))
            $this->response(array('errors' => $this->form_validation->error_array()));
        
        $this->load->model('Gift_model', 'gift');
        
        $result = $this->gift->send($data['user_id'], $data['sender_id'], $data['gift_id'], $data['text']);
        
        if($result['status'] == TRUE){
            $this->response(array(
                'message' => $result['records']
            ), 200);
        }else{
            $this->response(array(
                'errors' => $result['errors']
            ), $result['status'] != NULL ? $result['status'] : 422);
        }
    }
    
    /*
     * Add to blacklist
     */
    public function add_to_blacklist_post()
    {
        $data = array(
            'user_id' => $this->post('user_id'),
            'user' => $this->userID
        );
        
        $this->form_validation->set_data($data);
        
        if(!$this->form_validation->run('add_to_blacklist'))
            $this->response(array('errors' => $this->form_validation->error_array()));
        
        $this->load->model('Blacklist_model', 'blacklist');
        
        $result = $this->blacklist->add($data['user'], $data['user_id']);
        
        if($result['status'] == TRUE){
            $this->response(array(
                'message' => $result['message']
            ), 200);
        }else{
            $this->response(array(
                'errors' => $result['errors']
            ), $result['status'] != NULL ? $result['status'] : 422);
        }
    }
    
    /*
     * Remove user from blacklist
     */
    public function remove_from_blacklist_delete($user_id)
    {
        $data = array(
            'user_id' => $user_id,
            'user' => $this->userID
        );
        
        $this->form_validation->set_data($data);
        
        if(!$this->form_validation->run('remove_from_blacklist'))
            $this->response(array('errors' => $this->form_validation->error_array()));
        
        $this->load->model('Blacklist_model', 'blacklist');
        
        $result = $this->blacklist->remove($data['user'], $data['user_id']);
        
        if($result['status'] == TRUE){
            $this->response(array(
                'message' => $result['message']
            ), 200);
        }else{
            $this->response(array(
                'errors' => $result['errors']
            ), $result['status'] != NULL ? $result['status'] : 422);
        }
    }
    
    /*
     * Get user blacklist
     */
    public function blacklist_get()
    {
        $data = array(
            'user' => $this->userID
        );
        
        $this->form_validation->set_data($data);
        
        if(!$this->form_validation->run('get_blacklist'))
            $this->response(array('errors' => $this->form_validation->error_array()));
        
        $this->load->model('Blacklist_model', 'blacklist');
        
        $result = $this->blacklist->get($data['user']);
        
        if($result['status'] == TRUE){
            $this->response(array(
                'records' => $result['records']
            ), 200);
        }else{
            $this->response(array(
                'errors' => $result['errors']
            ), $result['status'] != NULL ? $result['status'] : 422);
        }
    }
    
    /*
     * Get top users
     */
    public function top_users_get()
    {
        $this->load->model('User_model', 'users');
        $this->response($this->users->top(), 200);
    }



    /*
     * Checking if current request has token in header
     *
     * */

    private function checkForToken()
    {
        $token = $this->input->get_request_header('X-API-KEY', TRUE);
        if($token){
            if(stripos($token, 'Bearer') !== FALSE){
                $token = str_replace('Bearer', '', $token);
                $this->load->library('encryption');
                $id = $this->encryption->decrypt(trim($token));
                if(!$id){
                    $this->response(array('errors' => ['Token is invalid!']), REST_Controller::HTTP_UNAUTHORIZED);
                }else{
                    $this->userID = $id;
                }
            }else{
                $this->response(array('errors' => ['Token is invalid!']), REST_Controller::HTTP_UNAUTHORIZED);
            }
        }else{
            $this->response(array('errors' => ['Token is invalid!']), REST_Controller::HTTP_UNAUTHORIZED);
        }
    }


}