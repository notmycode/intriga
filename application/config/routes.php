<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
| https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
| $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
| $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
| $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples: my-controller/index -> my_controller/index
|   my-controller/my-method -> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

/*
| -------------------------------------------------------------------------
| API
| -------------------------------------------------------------------------
*/
$route['api/v1/auth/registration']['post'] = 'api/api/registration';
$route['api/v1/auth']['post'] = 'api/api/login';


/*
 * Wall post API methods group
 * */
$route['api/v1/post/add/(:num)']['post'] = 'api/apiauth/create_post_wall/$1';
$route['api/v1/wall/(:num)']['get'] = 'api/apiauth/wall_posts/$1';
$route['api/v1/post/(:num)']['delete'] = 'api/apiauth/wall_post/$1';

/*
 * Friends API methods group
 * */
$route['api/v1/friend/add']['post'] = 'api/apiauth/add_friend';
$route['api/v1/friend/approve']['put'] = 'api/apiauth/approve_friend';
$route['api/v1/friend/requests']['get'] = 'api/apiauth/friends_requests';
$route['api/v1/friends/(:num)']['get'] = 'api/apiauth/friends/$1';
$route['api/v1/friends/decline']['post'] = 'api/apiauth/decline_friend';
$route['api/v1/friends/remove/(:num)']['delete'] = 'api/apiauth/remove_friend/$1';


/*
 * Albums API methods group
 * */
$route['api/v1/album']['post'] = 'api/apiauth/album_create';
$route['api/v1/album/(:num)']['put'] = 'api/apiauth/album_update/$1';
$route['api/v1/album/(:num)']['get'] = 'api/apiauth/album_get/$1';
$route['api/v1/album/(:num)']['delete'] = 'api/apiauth/album_remove/$1';
$route['api/v1/albums/(:num)']['get'] = 'api/apiauth/get_albums/$1';
$route['api/v1/photos/(:num)']['get'] = 'api/apiauth/get_photos/$1';

/*
 * Gift API methods group
 * */
$route['api/v1/gift']['post'] = 'api/apiauth/send_gift';
$route['api/v1/gifts/(:num)']['get'] = 'api/apiauth/user_gifts/$1';
$route['api/v1/gifts']['get'] = 'api/apiauth/gifts';

/*
 * Black list API methods group
 */
$route['api/v1/blacklist']['get'] = 'api/apiauth/blacklist';
$route['api/v1/blacklist']['post'] = 'api/apiauth/add_to_blacklist';
$route['api/v1/blacklist/(:num)']['delete'] = 'api/apiauth/remove_from_blacklist/$1';

/*
 * User API methods group
 */
$route['api/v1/user/search']['get'] = 'api/apiauth/search_user';
$route['api/v1/user/top']['get'] = 'api/apiauth/top_users';

//deelte in producation
$route['api/v1/status']['post'] = 'api/status/create';
$route['api/v1/status/(:num)']['get'] = 'api/status/single/$1';

