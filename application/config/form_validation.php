<?php
$config = array(
    'registration' => array(
        array(
            'field' => 'tel',
            'label' => 'tel',
            'rules' => 'required|is_unique[users.tel]'
        ),
        array(
            'field' => 'bday',
            'label' => 'Birthday',
            'rules' => 'required|valid_date'
        ),
        array(
            'field' => 'first_name',
            'label' => 'First name',
            'rules' => 'required'
        ),
        array(
            'field' => 'last_name',
            'label' => 'Last name',
            'rules' => 'required'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required|min_length[6]'
        ),
        array(
            'field' => 'password_confirm',
            'label' => 'Password confirm',
            'rules' => 'required|min_length[6]|matches[password]'
        )
    ),

    'auth' => array(
        array(
            'field' => 'tel',
            'label' => 'tel',
            'rules' => 'required'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required|min_length[6]'
        ),
    ),

    /*
     * Create a wall post
     * */

    'create_wall_post' => array(
        array(
            'field' => 'author_id',
            'label' => 'Author',
            'rules' => 'required|integer|is_exists_db[users.id]|in_blacklist[user_id]'
        ),

        array(
            'field' => 'user_id',
            'label' => 'User',
            'rules' => 'required|integer|is_exists_db[users.id]'
        ),
    ),

    /*
     *  Getting wall posts
     * */
    'get_wall_posts' => array(
        array(
            'field' => 'user_id',
            'label' => 'User',
            'rules' => 'required'
        ),

        array(
            'field' => 'user',
            'label' => 'User',
            'rules' => 'required|in_blacklist[user_id]',
        )
    ),

    /*
     * Delete post
     * */
    'delete_wall_post' => array(
        array(
            'field' => 'post_id',
            'label' => 'Post',
            'rules' => 'required|integer'
        ),
        array(
            'field' => 'user_id',
            'label' => 'User ID',
            'rules' => 'required|integer|is_exists_db[users.id]'
        )
    ),

    'add_friend' => array(
        array(
            'field' => 'user_id',
            'rules' => 'required|integer|is_exists_db[users.id]'
        ),

        array(
            'field' => 'user',
            'rules' => 'required|integer|must_not_be_like[user_id]|is_exists_db[users.id]|in_blacklist[user_id]'
        )
    ),

    'approve_friend' => array(
        array(
            'field' => 'request_id',
            'rules' => 'required|integer|is_exists_db[friends.id]'
        ),

        array(
            'field' => 'user_id',
            'rules' => 'required|integer|is_exists_db[users.id]'
        )
    ),

    'declime_friend' => array(
        array(
            'field' => 'request_id',
            'rules' => 'required|integer'
        ),

        array(
            'field' => 'user_id',
            'rules' => 'required|integer'
        )
    ),

    'remove_friend' => array(
        array(
            'field' => 'user',
            'rules' => 'required|integer'
        ),

        array(
            'field' => 'user_id',
            'rules' => 'required|integer|differs[user]'
        )
    ),

    'get_friends' => array(
        array(
            'field' => 'user',
            'rules' => 'required|integer|is_exists_db[users.id]|in_blacklist[user_id]'
        ),

        array(
            'field' => 'user_id',
            'rules' => 'required|integer'
        )
    ),

    'create_status' => array(
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'required|is_unique[statuses.name]'
        )
    ),

    /*
     * Albums validation group
     * */
    'create_album' => array(
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'required|max_length[250]'
        ),

        array(
            'field' => 'description',
            'label' => 'description',
            'rules' => 'max_length[255]'
        ),

        array(
            'field' => 'visibility',
            'label' => 'Visibility',
            'rules' => 'required|integer|in_list[0,1,2,3]'
        )
    ),

    'get_album' => array(
        array(
            'field' => 'user_id',
            'rules' => 'required|integer'
        )
    ),

    'get_albums' => array(
        array(
            'field' => 'user_id',
            'label' => 'User',
            'rules' => 'required|integer|is_exists_db[users.id]'
        ),

        array(
            'field' => 'user',
            'rules' => 'required|integer|in_blacklist[user_id]'
        )
    ),

    /*
     * Gifts validation group
     * */
    'get_user_gifts' => array(
        array(
            'field' => 'user_id',
            'label' => 'User',
            'rules' => 'required|integer'
        ),

        array(
            'field' => 'user',
            'label' => 'Sender',
            'rules' => 'required|integer|in_blacklist[user_id]'
        )
    ),

    'send_gift' => array(
        array(
            'field' => 'user_id',
            'label' => 'User reciver',
            'rules' => 'required|integer|differs[sender_id]|is_exists_db[users.id]'
        ),

        array(
            'field' => 'sender_id',
            'label' => 'Sender',
            'rules' => 'required|integer|in_blacklist[user_id]'
        ),
        
        array(
            'field' => 'gift_id',
            'label' => 'Gift ID',
            'rules' => 'required|integer|is_exists_db[gifts.id]'
        )
    ),
    
    'add_to_blacklist' => array(
        array(
            'field' => 'user_id',
            'label' => 'User',
            'rules' => 'required|integer|is_exists_db[users.id]|in_blacklist[user]'
        ),
        
        array(
            'field' => 'user',
            'label' => 'User',
            'rules' => 'required|integer|differs[user_id]' 
        )
    ),
    
    'remove_from_blacklist' => array(
        array(
            'field' => 'user_id',
            'label' => 'User',
            'rules' => 'required|integer'
        ),
        
        array(
            'field' => 'user',
            'label' => 'User current',
            'rules' => 'required|integer|differs[user_id]' 
        )
    ),
    
    'get_blacklist' => array(
        array(
            'field' => 'user',
            'label' => 'User current',
            'rules' => 'required|integer' 
        )
    ),

    'remove_album' => array(
        array(
            'field' => 'album_id',
            'label' => 'Album ID',
            'rules' => 'required|integer'
        )
    )
);