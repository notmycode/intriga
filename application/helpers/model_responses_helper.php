<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('response_error'))
{
    function response_error($errors, $statusCode)
    {
        return array(
            'status' => FALSE,
            'errors' => $errors,
            'code' => $statusCode
        );
    }
}

if ( ! function_exists('response_success'))
{
    function response_success($message, $statusCode)
    {
        return array(
            'status' => TRUE,
            'message' => $message,
            'code' => $statusCode
        );
    }
}

if ( ! function_exists('response_data'))
{
    function response_data($message, $statusCode)
    {
        return array(
            'status' => TRUE,
            'records' => $message,
            'code' => 200
        );
    }
}