/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50713
Source Host           : localhost:3306
Source Database       : intriga

Target Server Type    : MYSQL
Target Server Version : 50713
File Encoding         : 65001

Date: 2017-09-10 23:21:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for albums
-- ----------------------------
DROP TABLE IF EXISTS `albums`;
CREATE TABLE `albums` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `user_id` int(10) NOT NULL,
  `visibility` int(1) NOT NULL,
  `description` text NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `albums_fk0` (`user_id`),
  CONSTRAINT `albums_fk0` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for blacklists
-- ----------------------------
DROP TABLE IF EXISTS `blacklists`;
CREATE TABLE `blacklists` (
  `user_id` int(11) DEFAULT NULL,
  `blocked_id` int(11) DEFAULT NULL,
  KEY `user_id` (`user_id`),
  KEY `blocked_id` (`blocked_id`),
  CONSTRAINT `blacklists_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `blacklists_ibfk_2` FOREIGN KEY (`blocked_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `author_id` int(10) NOT NULL,
  `post_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_fk0` (`author_id`),
  CONSTRAINT `comments_fk0` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for finances
-- ----------------------------
DROP TABLE IF EXISTS `finances`;
CREATE TABLE `finances` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `additional` text NOT NULL,
  `sum` decimal(10,0) NOT NULL,
  `type` varchar(255) NOT NULL,
  `user_id` int(10) NOT NULL,
  `product` varchar(255) NOT NULL,
  `date` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `finances_fk0` (`user_id`),
  CONSTRAINT `finances_fk0` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for friends
-- ----------------------------
DROP TABLE IF EXISTS `friends`;
CREATE TABLE `friends` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_init` int(10) NOT NULL,
  `user_reciver` int(10) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `friends_fk0` (`user_init`),
  KEY `friends_fk1` (`user_reciver`),
  CONSTRAINT `friends_fk0` FOREIGN KEY (`user_init`) REFERENCES `users` (`id`),
  CONSTRAINT `friends_fk1` FOREIGN KEY (`user_reciver`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gifts
-- ----------------------------
DROP TABLE IF EXISTS `gifts`;
CREATE TABLE `gifts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `price` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gifts_relation
-- ----------------------------
DROP TABLE IF EXISTS `gifts_relation`;
CREATE TABLE `gifts_relation` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `gift_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `created_at` date NOT NULL,
  `status` int(1) NOT NULL,
  `sender_id` int(10) NOT NULL,
  `text` text,
  PRIMARY KEY (`id`),
  KEY `gifts_relation_fk0` (`gift_id`),
  KEY `gifts_relation_fk1` (`user_id`),
  KEY `gifts_relation_fk2` (`sender_id`),
  CONSTRAINT `gifts_relation_fk0` FOREIGN KEY (`gift_id`) REFERENCES `gifts` (`id`),
  CONSTRAINT `gifts_relation_fk1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `gifts_relation_fk2` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `is_active` int(1) NOT NULL,
  `public` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for images
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `album_id` int(10) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `src` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `images_fk0` (`album_id`),
  CONSTRAINT `images_fk0` FOREIGN KEY (`album_id`) REFERENCES `albums` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for likes
-- ----------------------------
DROP TABLE IF EXISTS `likes`;
CREATE TABLE `likes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `likes_fk0` (`user_id`),
  CONSTRAINT `likes_fk0` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `creator_is` int(10) NOT NULL,
  `parent_message` int(10) NOT NULL,
  `text` text NOT NULL,
  `attachments` text NOT NULL,
  `is_readed` int(1) NOT NULL DEFAULT '0',
  `reciver` int(10) NOT NULL,
  `group_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `messages_fk0` (`creator_is`),
  KEY `messages_fk1` (`reciver`),
  KEY `messages_fk2` (`group_id`),
  CONSTRAINT `messages_fk0` FOREIGN KEY (`creator_is`) REFERENCES `users` (`id`),
  CONSTRAINT `messages_fk1` FOREIGN KEY (`reciver`) REFERENCES `users` (`id`),
  CONSTRAINT `messages_fk2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for news_feed
-- ----------------------------
DROP TABLE IF EXISTS `news_feed`;
CREATE TABLE `news_feed` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `description` text NOT NULL,
  `date` timestamp NOT NULL,
  `image` text NOT NULL,
  `additional` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `news_feed_fk0` (`user_id`),
  CONSTRAINT `news_feed_fk0` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `author_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `text` text NOT NULL,
  `attachments` text NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `posts_fk0` (`author_id`),
  KEY `posts_fk1` (`user_id`),
  CONSTRAINT `posts_fk0` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`),
  CONSTRAINT `posts_fk1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for statuses
-- ----------------------------
DROP TABLE IF EXISTS `statuses`;
CREATE TABLE `statuses` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `balance` int(10) NOT NULL,
  `acive` int(10) NOT NULL,
  `role` int(10) NOT NULL,
  `bday` date DEFAULT NULL,
  `create_at` date DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_fk0` (`status`),
  KEY `users_fk1` (`role`),
  CONSTRAINT `users_fk0` FOREIGN KEY (`status`) REFERENCES `statuses` (`id`),
  CONSTRAINT `users_fk1` FOREIGN KEY (`role`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for users_settings_premission
-- ----------------------------
DROP TABLE IF EXISTS `users_settings_premission`;
CREATE TABLE `users_settings_premission` (
  `user_id` int(11) DEFAULT NULL,
  `premission_type` varchar(255) DEFAULT NULL,
  `value` int(1) DEFAULT NULL,
  KEY `user_id` (`user_id`),
  CONSTRAINT `users_settings_premission_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
